//
//  ContactTableViewCell.swift
//  Boost
//
//  Created by Jack Xiong Lim on 13/9/20.
//  Copyright © 2020 JXLim. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    
    static let ReuseIdentifier = String(describing: ContactTableViewCell.self)
    static let NibName = String(describing: ContactTableViewCell.self)
    
    @IBOutlet weak var contactImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contactImageView.layer.cornerRadius = self.contactImageView.bounds.width / 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
