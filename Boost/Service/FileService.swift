//
//  FileService.swift
//  Boost
//
//  Created by Jack Xiong Lim on 13/9/20.
//  Copyright © 2020 JXLim. All rights reserved.
//

import Foundation

class FileService {
    func getFile() -> URL? {
        return Bundle.main.url(forResource: "data", withExtension: "json")
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
}
