//
//  JsonService.swift
//  Boost
//
//  Created by Jack Xiong Lim on 13/9/20.
//  Copyright © 2020 JXLim. All rights reserved.
//

import Foundation

class JsonService {
    func getContacts() -> [Contact] {
        let fileService = FileService()
        if let url = fileService.getFile() {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let contacts = try decoder.decode([Contact].self, from: data)
                return contacts
            } catch {
                print("error:\(error)")
            }
        }
        return []
    }
}
