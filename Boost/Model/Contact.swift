//
//  Contact.swift
//  Boost
//
//  Created by Jack Xiong Lim on 13/9/20.
//  Copyright © 2020 JXLim. All rights reserved.
//

import Foundation

class Contact: Codable {
    let id: String
    let firstName: String
    let lastName: String
    let email: String?
    let phone: String?
    
    init(id: String, firstName: String, lastName: String, email: String?, phone: String?) {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phone = phone
    }
    
}

