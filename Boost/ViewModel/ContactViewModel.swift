//
//  ContactViewModel.swift
//  Boost
//
//  Created by Jack Xiong Lim on 13/9/20.
//  Copyright © 2020 JXLim. All rights reserved.
//

import Foundation

class ContactViewModel {
    private let contact: Contact
    
    init(contact: Contact?) {
        self.contact = contact ?? Contact(id: "", firstName: "", lastName: "", email: nil, phone: nil)
    }
    var firstName: String {
        return contact.firstName
    }
    
    var lastName: String {
        return contact.lastName
    }
    
    var email: String? {
        return contact.email
    }
    
    var phone: String? {
        return contact.phone
    }
    
}

extension ContactViewModel {
    func configure(_ cell: ContactTableViewCell) {
        cell.nameLabel.text = "\(firstName) \(lastName)"
    }
    
    typealias Changes = (firstName: String, lastName: String, email: String, phone: String)
    
    func update(with changes: Changes) {
      // Apply changes
      //    let updatedModel = contact
      //    updatedModel.firstName = changes.firstName
      //    updatedModel.lastName = changes.lastName
      //    updatedModel.email = changes.email
      //    updatedModel.phone = changes.phone
      
    }
    
    func writeToFile() {
      if let encodedData = try? JSONEncoder().encode(self.contact) {
        do {
          try encodedData.write(to: FileService().getFile()!)
        }
        catch {
          print("Failed to write JSON data: \(error.localizedDescription)")
        }
      }
    }

    
}

