//
//  ViewController.swift
//  Boost
//
//  Created by Jack Xiong Lim on 13/9/20.
//  Copyright © 2020 JXLim. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var contactVMs = [ContactViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setup()
    }
    
    func setup() {
        self.prepareTableView()
        let jsonService = JsonService()
        for contact in jsonService.getContacts() {
            self.contactVMs.append(ContactViewModel(contact: contact))
        }
    }
    
    func prepareTableView() {
        self.tableView.register(UINib(nibName: ContactTableViewCell.NibName, bundle: .main), forCellReuseIdentifier: ContactTableViewCell.ReuseIdentifier)
    }
    
    @IBAction func add(_ sender: UIBarButtonItem) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationNavVC = segue.destination as? UINavigationController, let destination = destinationNavVC.viewControllers.first as? ContactDetailTableViewController {
            destination.contact = sender as? ContactViewModel
        }
    }
    
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactVMs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:ContactTableViewCell.ReuseIdentifier, for: indexPath) as! ContactTableViewCell
        self.contactVMs[indexPath.row].configure(cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: ContactDetailTableViewController.ReuseIdentifier, sender: self.contactVMs[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
