//
//  ContactDetailTableViewController.swift
//  Boost
//
//  Created by Jack Xiong Lim on 13/9/20.
//  Copyright © 2020 JXLim. All rights reserved.
//

import UIKit
class ContactDetailTableViewController: UITableViewController {
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    static let ReuseIdentifier = String(describing: ContactDetailTableViewController.self)
    var contact: ContactViewModel!
    var firstLoad = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }
    
    func setup() {
        self.imageView.layer.cornerRadius = self.imageView.bounds.width / 2
        
        self.tableView.tableFooterView = UIView()
        
        self.firstNameTextField.text = self.contact.firstName
        self.lastNameTextField.text = self.contact.lastName
        self.emailTextField.text = self.contact.email
        self.phoneTextField.text = self.contact.phone
        self.checkIfSaveButtonShouldBeEnabled()
    }
    
    func checkIfSaveButtonShouldBeEnabled() {
        self.saveButton.isEnabled = ((self.firstNameTextField.text ?? "").count > 0) && ((self.lastNameTextField.text ?? "").count > 0)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !self.firstLoad {
            self.firstNameTextField.translatesAutoresizingMaskIntoConstraints = false
            self.emailTextField.translatesAutoresizingMaskIntoConstraints = false
            self.phoneTextField.translatesAutoresizingMaskIntoConstraints = false
            
            self.firstNameTextField.widthAnchor.constraint(equalTo: lastNameTextField.widthAnchor, multiplier: 1).isActive = true
            self.emailTextField.widthAnchor.constraint(equalTo: lastNameTextField.widthAnchor, multiplier: 1).isActive = true
            self.phoneTextField.widthAnchor.constraint(equalTo: lastNameTextField.widthAnchor, multiplier: 1).isActive = true
            self.firstLoad = true
        }
        
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func type(_ sender: UITextField) {
        self.checkIfSaveButtonShouldBeEnabled()
    }
    
    @IBAction func save(_ sender: UIBarButtonItem) {
    }
    
    
}

extension ContactDetailTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case self.firstNameTextField:
            self.lastNameTextField.becomeFirstResponder()
        case self.lastNameTextField:
            self.emailTextField.becomeFirstResponder()
        case self.emailTextField:
            self.phoneTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        
        return true
    }
    
}
