//
//  BoostTests.swift
//  BoostTests
//
//  Created by Jack Xiong Lim on 13/9/20.
//  Copyright © 2020 JXLim. All rights reserved.
//

import XCTest
@testable import Boost

class BoostTests: XCTestCase {
    let fileService = FileService()
    let jsonService = JsonService()
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testJsonFileExists() {
        XCTAssertNotNil(fileService.getFile(), "Data file not found")
    }
    
    func testValidJson() {
        XCTAssertNotEqual(jsonService.getContacts().count, 0, "No Contacts")
    }
    
    
}
